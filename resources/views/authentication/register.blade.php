<!DOCTYPE html>
<html lang="en">
<head>
  <title>Role</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Register</h2>
  <form class="form-horizontal" action="{{route('postRegister')}}" method="post">
    {!! csrf_field() !!}
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="first_name">First name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="first_name" placeholder="Enter first name" name="first_name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="last_name">Last name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="last_name" placeholder="Enter last name" name="last_name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="location">Location:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="location" placeholder="Enter location" name="location">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-10">
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password confirm:</label>
      <div class="col-sm-10">
        <input type="password" class="form-control" id="pwdConf" placeholder="Enter password" name="password_confirmation">
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
</div>

</body>
</html>
